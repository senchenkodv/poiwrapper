package com.elinext.poi_wrapper.excel;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Sheet;

/**
 * The interface Style editor. Applies the selected style to a cell or set of cells.
 */
public interface StyleEditor {
    /**
     * Sets row style. Applies a style by id to a row.
     *
     * @param sheet     the sheet
     * @param rowId     the row id
     * @param cellStyle the cell style
     */
    void setRowStyle(Sheet sheet, int rowId, CellStyle cellStyle);

    /**
     * Sets rows style. Applies a style to a range of rows by id.
     * Extreme values are included in the set.
     *
     * @param sheet     the sheet
     * @param rowFrom   the row from
     * @param rowTo     the row to
     * @param cellStyle the cell style
     */
    void setRowsStyle(Sheet sheet, int rowFrom, int rowTo, CellStyle cellStyle);

    /**
     * Sets column style. Applies a style by id to a column.
     *
     * @param sheet     the sheet
     * @param columnId  the column id
     * @param cellStyle the cell style
     */
    void setColumnStyle(Sheet sheet, int columnId, CellStyle cellStyle);

    /**
     * Sets columns style. Applies a style to a range of columns by id.
     * Extreme values are included in the set.
     *
     * @param sheet      the sheet
     * @param columnFrom the column from
     * @param columnTo   the column to
     * @param cellStyle  the cell style
     */
    void setColumnsStyle(Sheet sheet, int columnFrom, int columnTo, CellStyle cellStyle);

    /**
     * Sets cell style. Applies a style to a cell by id.
     *
     * @param cell      the cell
     * @param cellStyle the cell style
     */
    void setCellStyle(Cell cell, CellStyle cellStyle);
}