package com.elinext.poi_wrapper.repository;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * The interface Style creation.
 */
public interface StyleCreation {
    /**
     * Header style cell style. Returns a specific style for the table header.
     * Can be used to style other cells.
     *
     * @param wb the wb
     * @return the cell style
     */
    CellStyle headerStyle(Workbook wb);
}
