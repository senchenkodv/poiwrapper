package com.elinext.poi_wrapper.excel;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;

/**
 * The interface Style builder. Contains methods for constructing a cell style.
 * After the construction of the style is complete, the build() method should be called.
 */
public interface StyleBuilder {
    /**
     * Get editor cell style editor.
     *
     * @param cellStyle the cell style
     * @return the cell style editor
     */
    static CellStyleEditor getEditor(CellStyle cellStyle){
        return null;
    }

    /**
     * Horizontal alignment cell style editor.
     *
     * @param horizontalAlignment the horizontal alignment
     * @return the cell style editor
     */
    CellStyleEditor horizontalAlignment(HorizontalAlignment horizontalAlignment);

    /**
     * Border bottom cell style editor.
     *
     * @param borderStyle the border style
     * @return the cell style editor
     */
    CellStyleEditor borderBottom(BorderStyle borderStyle);

    /**
     * Border left cell style editor.
     *
     * @param borderStyle the border style
     * @return the cell style editor
     */
    CellStyleEditor borderLeft(BorderStyle borderStyle);

    /**
     * Border right cell style editor.
     *
     * @param borderStyle the border style
     * @return the cell style editor
     */
    CellStyleEditor borderRight(BorderStyle borderStyle);

    /**
     * Border top cell style editor.
     *
     * @param borderStyle the border style
     * @return the cell style editor
     */
    CellStyleEditor borderTop(BorderStyle borderStyle);

    /**
     * Bottom border color cell style editor.
     *
     * @param color the color
     * @return the cell style editor
     */
    CellStyleEditor bottomBorderColor(short color);

    /**
     * Background color cell style editor.
     *
     * @param backgroundColor the background color
     * @return the cell style editor
     */
    CellStyleEditor backgroundColor(short backgroundColor);

    /**
     * Data format cell style editor.
     *
     * @param dataFormat the data format
     * @return the cell style editor
     */
    CellStyleEditor dataFormat(short dataFormat);

    /**
     * Font cell style editor.
     *
     * @param font the font
     * @return the cell style editor
     */
    CellStyleEditor font(Font font);

    /**
     * Build cell style.
     *
     * @return the cell style
     */
    CellStyle build();
}
