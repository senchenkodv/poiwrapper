package com.elinext.poi_wrapper.excel;

import org.apache.poi.ss.usermodel.CellStyle;

/**
 * The type Excel cell wrapper.
 */
public class ExcelCellWrapper implements CellWrapper{
    private Object value;
    private CellStyle style;

    @Override
    public Object getValue() {
        return value;
    }

    @Override
    public CellStyle getStyle() {
        return style;
    }

    @Override
    public CellWrapper setValue(Object value) {
        this.value = value;
        return this;
    }

    @Override
    public CellWrapper setStyle(CellStyle style) {
        this.style = style;
        return this;
    }

    @Override
    public CellWrapper setValueAndStyle(Object value, CellStyle style) {
        this.value = value;
        this.style = style;
        return this;
    }
}
