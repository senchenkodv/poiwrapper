package com.elinext.poi_wrapper.excel;

import org.apache.poi.ss.usermodel.*;

/**
 * The type Cell style editor.
 */
public class CellStyleEditor implements StyleBuilder{
    private CellStyle cellStyle;

    private CellStyleEditor(CellStyle cellStyle) {
        this.cellStyle = cellStyle;
    }

    /**
     * Get editor cell style editor.
     *
     * @param cellStyle the cell style
     * @return the cell style editor
     */
    public static CellStyleEditor getEditor(CellStyle cellStyle){
        return new CellStyleEditor(cellStyle);
    }

    @Override
    public CellStyleEditor horizontalAlignment(HorizontalAlignment horizontalAlignment) {
        this.cellStyle.setAlignment(horizontalAlignment);
        return this;
    }

    @Override
    public CellStyleEditor borderBottom(BorderStyle borderStyle) {
        this.cellStyle.setBorderBottom(borderStyle);
        return this;
    }

    @Override
    public CellStyleEditor borderLeft(BorderStyle borderStyle) {
        this.cellStyle.setBorderLeft(borderStyle);
        return this;
    }

    @Override
    public CellStyleEditor borderRight(BorderStyle borderStyle) {
        this.cellStyle.setBorderRight(borderStyle);
        return this;
    }

    @Override
    public CellStyleEditor borderTop(BorderStyle borderStyle) {
        this.cellStyle.setBorderTop(borderStyle);
        return this;
    }

    @Override
    public CellStyleEditor bottomBorderColor(short color) {
        this.cellStyle.setBottomBorderColor(color);
        return this;
    }

    @Override
    public CellStyleEditor backgroundColor(short backgroundColor) {
        this.cellStyle.setFillBackgroundColor(backgroundColor);
        return this;
    }

    @Override
    public CellStyleEditor dataFormat(short dataFormat) {
        this.cellStyle.setDataFormat(dataFormat);
        return this;
    }

    @Override
    public CellStyleEditor font(Font font) {
        this.cellStyle.setFont(font);
        return this;
    }

    @Override
    public CellStyle build() {
        return this.cellStyle;
    }
}
