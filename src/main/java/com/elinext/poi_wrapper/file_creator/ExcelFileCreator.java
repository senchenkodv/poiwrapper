package com.elinext.poi_wrapper.file_creator;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * The type Excel file creator.
 */
public class ExcelFileCreator implements FileCreator{
    private static final Logger LOGGER = LogManager.getLogger();
    private static final String SLSX = "%s.xlsx";

    @Override
    public void createXlsxFile(XSSFWorkbook workbook, String fileName){
        try (FileOutputStream outputStream = new FileOutputStream(String.format(SLSX, fileName))){
            workbook.write(outputStream);
        } catch (FileNotFoundException e) {
            LOGGER.log(Level.ERROR, "File not found", e);
        } catch (IOException e) {
            LOGGER.log(Level.ERROR, "I/O exception", e);
        }
    }
}