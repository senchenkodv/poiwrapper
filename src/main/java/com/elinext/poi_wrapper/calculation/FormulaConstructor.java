package com.elinext.poi_wrapper.calculation;

/**
 * The type Formula constructor.
 */
public class FormulaConstructor implements ExcelCalculation {

    @Override
    public String sum(String fromCell, String toCell){
       return  String.format("SUM(%s:%s)", fromCell, toCell);
    }

    @Override
    public String average(String fromCell, String toCell){
        return  String.format("AVERAGE(%s:%s)", fromCell, toCell);
    }

    @Override
    public String count(String fromCell, String toCell){
        return  String.format("COUNT(%s:%s)", fromCell, toCell);
    }

    @Override
    public String counta(String fromCell, String toCell){
        return  String.format("COUNTA(%s:%s)", fromCell, toCell);
    }

    @Override
    public String ifCondition(String arg1, String arg2){
        return  String.format("IF(%s<%s, ‘TRUE,’ ‘FALSE’)", arg1, arg2);
    }

    @Override
    public String sum(String cell){
        return  String.format("TRIM(%s)", cell);
    }

    @Override
    public String max(String fromCell, String toCell){
        return  String.format("MAX(%s:%s)", fromCell, toCell);
    }

    @Override
    public String min(String fromCell, String toCell){
        return  String.format("MIN(%s:%s)", fromCell, toCell);
    }
}
