package com.elinext.poi_wrapper.excel;

import org.apache.poi.ss.usermodel.CellStyle;

/**
 * The interface Cell wrapper.
 *
 * @param <V> the type parameter
 * @param <S> the type parameter
 */
public interface CellWrapper<V,S extends CellStyle> {

    /**
     * Gets value.
     *
     * @return the value
     */
    V getValue();

    /**
     * Gets style.
     *
     * @return the style
     */
    S getStyle();

    /**
     * Sets value.
     *
     * @param value the value
     * @return the value
     */
    CellWrapper setValue(V value);

    /**
     * Sets style.
     *
     * @param style the style
     * @return the style
     */
    CellWrapper setStyle(S style);

    /**
     * Sets value and style.
     *
     * @param value the value
     * @param style the style
     * @return the value and style
     */
    CellWrapper setValueAndStyle(V value, S style);
}
