package com.elinext.poi_wrapper.excel;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.util.Iterator;

/**
 * The type Workbook style editor.
 */
public class WorkbookStyleEditor implements StyleEditor{

    @Override
    public void setRowStyle(Sheet sheet, int rowId, CellStyle cellStyle){
        Row row = sheet.getRow(rowId);
        Iterator<Cell> cellIterator = row.cellIterator();
        cellIterator.forEachRemaining(cell -> cell.setCellStyle(cellStyle));
    }

    @Override
    public void setRowsStyle(Sheet sheet, int rowFrom, int rowTo, CellStyle cellStyle){
        for (int i = rowFrom; i <= rowTo; i++) {
            Row row = sheet.getRow(i);
            Iterator<Cell> cellIterator = row.cellIterator();
            cellIterator.forEachRemaining(cell -> cell.setCellStyle(cellStyle));
        }
    }

    @Override
    public void setColumnStyle(Sheet sheet, int columnId, CellStyle cellStyle) {
        for (int i = 0; i <= sheet.getLastRowNum(); i++) {
            Row row = sheet.getRow(i);
            row.getCell(columnId).setCellStyle(cellStyle);
        }
    }

    @Override
    public void setColumnsStyle(Sheet sheet, int columnFrom, int columnTo, CellStyle cellStyle){
        for (int i = 0; i <= sheet.getLastRowNum(); i++) {
            Row row = sheet.getRow(i);
            for (int j = columnFrom; j <= columnTo; j++) {
                row.getCell(j).setCellStyle(cellStyle);
            }
        }
    }

    @Override
    public void setCellStyle(Cell cell, CellStyle cellStyle) {
        cell.setCellStyle(cellStyle);
    }
}
