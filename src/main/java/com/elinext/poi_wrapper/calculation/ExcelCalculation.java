package com.elinext.poi_wrapper.calculation;

/**
 * The interface Excel calculation. Performs the generation of table formulas.
 * The resulting string can be used to set the formula for the cell.
 */
public interface ExcelCalculation {
    /**
     * Sum string.
     *
     * @param fromCell the from cell
     * @param toCell   the to cell
     * @return the string
     */
    String sum(String fromCell, String toCell);

    /**
     * Average string.
     *
     * @param fromCell the from cell
     * @param toCell   the to cell
     * @return the string
     */
    String average(String fromCell, String toCell);

    /**
     * Count string.
     *
     * @param fromCell the from cell
     * @param toCell   the to cell
     * @return the string
     */
    String count(String fromCell, String toCell);

    /**
     * Counta string.
     *
     * @param fromCell the from cell
     * @param toCell   the to cell
     * @return the string
     */
    String counta(String fromCell, String toCell);

    /**
     * If condition string.
     *
     * @param arg1 the arg 1
     * @param arg2 the arg 2
     * @return the string
     */
    String ifCondition(String arg1, String arg2);

    /**
     * Sum string.
     *
     * @param cell the cell
     * @return the string
     */
    String sum(String cell);

    /**
     * Max string.
     *
     * @param fromCell the from cell
     * @param toCell   the to cell
     * @return the string
     */
    String max(String fromCell, String toCell);

    /**
     * Min string.
     *
     * @param fromCell the from cell
     * @param toCell   the to cell
     * @return the string
     */
    String min(String fromCell, String toCell);
}
