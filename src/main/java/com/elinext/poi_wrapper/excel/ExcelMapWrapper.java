package com.elinext.poi_wrapper.excel;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * The type Excel map wrapper.
 */
public class ExcelMapWrapper implements WorkbookWrapper {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final String SLSX = "%s.xlsx";
    private Map<String, Map<String, CellWrapper>> mapTable;
    private XSSFWorkbook workbook;
    private XSSFSheet sheet;

    @NotNull
    public XSSFWorkbook getWorkbook() {
        return workbook;
    }

    private ExcelMapWrapper(String sheetName) {
        this.workbook = new XSSFWorkbook();
        this.mapTable = new LinkedHashMap<>();
        this.sheet = workbook.createSheet(sheetName);
    }

    /**
     * Create table workbook wrapper.
     *
     * @param sheetName the sheet name
     * @return the workbook wrapper
     */
    @NotNull
    public static WorkbookWrapper createTable(@NotNull String sheetName) {
        return new ExcelMapWrapper(sheetName);
    }

    @Override
    @Nullable
    public Map<String, CellWrapper> getRow(@NotNull String rowName) {
        return mapTable.get(rowName);
    }

    @Override
    @Nullable
    public Map<String, CellWrapper> getColumn(@NotNull String columnName) {
        return mapTable.values().stream().anyMatch(column -> column.get(columnName) != null) ?
                mapTable.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey,
                value -> value.getValue().get(columnName))) : null;
    }

    @Override
    @NotNull
    public WorkbookWrapper addNextRow(@NotNull String rowName) {
        if (mapTable.isEmpty()) {
            mapTable.put(rowName, new LinkedHashMap<>());
        } else {
            mapTable.put(rowName, mapTable.values().stream().findFirst().get().keySet()
                    .stream().collect(Collectors.toMap(Function.identity(), cell -> new ExcelCellWrapper())));
        }
        return this;
    }

    @Override
    @NotNull
    public WorkbookWrapper addNextColumn(@NotNull String columnName) {
        mapTable.values().forEach(row -> row.put(columnName, new ExcelCellWrapper()));
        return this;
    }

    @Override
    @Nullable
    public CellWrapper getCell(@NotNull String rowName, @NotNull String columnName) {
        return mapTable.get(rowName) != null ?  mapTable.get(rowName).get(columnName) : null;
    }

    @Override
    @NotNull
    public WorkbookWrapper setRowStyle(@NotNull String rowName, @NotNull CellStyle cellStyle) {
        Map<String, CellWrapper> cellWrapperMap = mapTable.get(rowName);
        if (cellWrapperMap != null){
            cellWrapperMap.values().forEach(row -> row.setStyle(cellStyle));
        }
        return this;
    }

    @Override
    @NotNull
    public WorkbookWrapper setRowsStyle(@NotNull List rowNames, @NotNull CellStyle cellStyle) {
        rowNames.forEach(row -> setRowStyle((String) row, cellStyle));
        return this;
    }

    @Override
    @NotNull
    public WorkbookWrapper setColumnStyle(@NotNull String columnName, @NotNull CellStyle cellStyle) {
        Map<String, CellWrapper> cellWrapperMap = getColumn(columnName);
        if (cellWrapperMap != null){
            cellWrapperMap.values().forEach(column -> column.setStyle(cellStyle));
        }
        return this;
    }

    @Override
    @NotNull
    public WorkbookWrapper setColumnsStyle(@NotNull List columnsNames, @NotNull CellStyle cellStyle) {
        columnsNames.forEach(column -> setColumnStyle((String) column, cellStyle));
        return this;
    }


    @Override
    @NotNull
    public WorkbookWrapper setCellStyle(@NotNull String rowName, @NotNull String columnName, @NotNull CellStyle cellStyle) {
        CellWrapper cellWrapper = mapTable.get(rowName).get(columnName);
        if (cellWrapper != null){
            cellWrapper.setStyle(cellStyle);
        }
        return this;
    }

    @Override
    public void exportToXlsx(@NotNull String fileName) {
        int rowNum = 0;
        Iterator<Map<String, CellWrapper>> rowIterator = mapTable.values().iterator();
        while (rowIterator.hasNext()) {
            Map<String, CellWrapper> next = rowIterator.next();
            XSSFRow row = sheet.createRow(rowNum++);
            int colNum = 0;
            Iterator<CellWrapper> columnIterator = next.values().iterator();
            while (columnIterator.hasNext()) {
                CellWrapper cellWrapper = columnIterator.next();
                Object value = cellWrapper.getValue();
                CellStyle style = cellWrapper.getStyle();
                XSSFCell cell = row.createCell(colNum++);

                if (value instanceof String) {
                    cell.setCellValue((String) value);
                } else if (value instanceof Integer) {
                    cell.setCellValue((Integer) value);
                } else if (value instanceof Double){
                    cell.setCellValue((Double) value);
                }
                cell.setCellStyle(style);
            }
        }
        writeToFile(fileName);
    }

    private void writeToFile(@NotNull String fileName) {
        try (FileOutputStream outputStream = new FileOutputStream(String.format(SLSX, fileName))){
            workbook.write(outputStream);
        } catch (FileNotFoundException e) {
            LOGGER.log(Level.ERROR, "File not found", e);
        } catch (IOException e) {
            LOGGER.log(Level.ERROR, "I/O exception", e);
        }
    }
}
