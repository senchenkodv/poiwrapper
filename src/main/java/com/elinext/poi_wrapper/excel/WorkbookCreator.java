package com.elinext.poi_wrapper.excel;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * The type Workbook creator.
 */
class WorkbookCreator implements TableCreator{

    private void fillSheet(@NotNull Object[][] data, XSSFSheet sheet) {
        int rowNum = 0;
        for (Object[] value : data) {
            XSSFRow row = sheet.createRow(rowNum++);
            int colNum = 0;
            for (Object field : value) {
                XSSFCell cell = row.createCell(colNum++);

                if (field instanceof String) {
                    cell.setCellValue((String) field);
                } else if (field instanceof Integer) {
                    cell.setCellValue((Integer) field);
                } else if (field instanceof Double){
                    cell.setCellValue((Double) field);
                }
            }
        }
    }

    @Override
    public XSSFWorkbook createWorkbook(@NotNull Object[][] data, String sheetName) {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet(sheetName);
        fillSheet(data, sheet);
        return workbook;
    }

    @Override
    public XSSFWorkbook createWorkbook(@NotNull List<List<Object>> table, String sheetName) {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet(sheetName);
        Object[][] arrayTable = new Object[table.size()][];
        int i = 0;
        for (List<Object> list : table) {
            arrayTable[i++] = list.toArray(new Object[list.size()]);
        }
        fillSheet(arrayTable, sheet);
        return workbook;
    }
}