package com.elinext.poi_wrapper.excel;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * The interface Table creator.
 */
public interface TableCreator {
    /**
     * Create workbook xssf workbook. Creates a table based on data in a multidimensional array.
     *
     * @param data      the data
     * @param sheetName the sheet name
     * @return the xssf workbook
     */
    XSSFWorkbook createWorkbook(@NotNull Object[][] data, String sheetName);

    /**
     * Create workbook xssf workbook. Creates a table based on the data in the list of lists.
     *
     * @param table     the table
     * @param sheetName the sheet name
     * @return the xssf workbook
     */
    XSSFWorkbook createWorkbook(@NotNull List<List<Object>> table, String sheetName);
}
