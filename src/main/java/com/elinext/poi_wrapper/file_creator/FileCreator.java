package com.elinext.poi_wrapper.file_creator;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * The interface File creator.
 */
public interface FileCreator {
    /**
     * Create xlsx file.
     *
     * @param workbook the workbook
     * @param fileName the file name
     */
    void createXlsxFile(XSSFWorkbook workbook, String fileName);
}
