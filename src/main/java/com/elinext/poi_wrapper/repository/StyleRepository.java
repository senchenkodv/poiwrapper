package com.elinext.poi_wrapper.repository;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFFont;

/**
 * The type Style repository.
 */
public class StyleRepository implements StyleCreation {

    @Override
    public CellStyle headerStyle(Workbook wb) {
        CellStyle style = wb.createCellStyle();
        style.setFillBackgroundColor(IndexedColors.RED.getIndex());
        XSSFFont font = (XSSFFont) wb.createFont();
        font.setBold(true);
        font.setColor(IndexedColors.RED.getIndex());
        style.setFont(font);
        style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
        return style;
    }
}
